package net.jmud.db;

import lombok.Data;
import net.jmud.types.PrototypeId;

import java.util.ArrayList;
import java.util.List;

@Data
public class PrototypeDto {
    private PrototypeId id;
    private String name;
    private String keywords;
    private List<PrototypeExitDto> exits = new ArrayList<>();
}
