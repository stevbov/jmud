package net.jmud.db;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ZoneDb {
    public ZoneDb(Path directory) {
        this.directory = directory;
    }

    public List<ZoneDto> loadAll() {
        List<ZoneDto> zones = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        try (DirectoryStream<Path> ds = Files.newDirectoryStream(directory)) {
            for (Path child : ds) {
                log.debug("Processing file [{}]", child);
                if (child.getFileName().toString().endsWith(".json")) {
                    ZoneDto zone = mapper.readValue(child.toFile(), ZoneDto.class);
                    log.debug("  Loaded zone data from file [{}]: {}", child, zone);
                    zones.add(zone);
                } else {
                    log.debug("  Skipping");
                }
            }
        } catch (IOException e) {
            throw new DbException(e);
        }

        return zones;
    }

    public void save(ZoneDto zone) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(directory.resolve(zone.getId() + ".json").toFile(), zone);
        } catch (IOException e) {
            throw new DbException(e);
        }
    }

    public void saveAll(List<ZoneDto> zones) {
        for (ZoneDto zone : zones) {
            save(zone);
        }
    }

    private final Path directory;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
}
