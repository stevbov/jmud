package net.jmud.db;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ZoneDto {
    private int id;
    private String name;
    private List<PrototypeDto> rooms = new ArrayList<>();
}
