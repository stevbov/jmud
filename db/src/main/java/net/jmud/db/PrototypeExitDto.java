package net.jmud.db;

import lombok.Data;
import net.jmud.types.Direction;
import net.jmud.types.PrototypeId;

@Data
public class PrototypeExitDto {
    private PrototypeId to;
    private Direction direction;
}
