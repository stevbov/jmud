package net.jmud.db;

public class DbException extends RuntimeException {
    public DbException(Exception e) {
        super(e);
    }
}
