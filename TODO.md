* effects
* entity stats, flags
* skills
* powers
* wearable items
    * item slots
    * wear command
    * look command should show them

# Commands
* say command: use actions so NPCs can see it
* chat command: should go only to other players.  Maybe build generic channel and chat module that can be used for groups, clans, etc.
* show zones command: list all zones

# Online Creation
## Rooms
* create room prototypes
* link room prototypes
* delete room prototypes

## Items
* create item prototypes
* delete item prototypes
* show item protoypes

## Creatures
* create creature prototypes
* delete creature prototypes
* show creature protoypes

## Resets
* item resets
* creature resets

