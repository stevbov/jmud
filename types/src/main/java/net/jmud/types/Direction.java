package net.jmud.types;

import lombok.Getter;

public enum Direction {
    NORTH("north") {
        @Override
        public Direction getReverse() {
            return SOUTH;
        }
    },
    SOUTH("south") {
        @Override
        public Direction getReverse() {
            return NORTH;
        }
    },
    EAST("east") {
        @Override
        public Direction getReverse() {
            return WEST;
        }
    },
    WEST("west") {
        @Override
        public Direction getReverse() {
            return EAST;
        }
    },
    UP("up") {
        @Override
        public Direction getReverse() {
            return DOWN;
        }
    },
    DOWN("down") {
        @Override
        public Direction getReverse() {
            return UP;
        }
    };

    Direction(String name) {
        this.name = name;
    }

    public abstract Direction getReverse();

    public static Direction parse(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }

        str = str.toLowerCase();

        for (Direction direction : values()) {
            if (direction.getName().startsWith(str)) {
                return direction;
            }
        }

        return null;
    }

    @Getter
    private final String name;
}
