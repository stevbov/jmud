package net.jmud.types;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

@Data
public class PrototypeId {
    @Override
    @JsonValue
    public String toString() {
        return zoneId + "-" + id;
    }

    public EntityId firstEntityId() {
        return toEntityId(1);
    }

    public EntityId toEntityId(int num) {
        if (num == 1) {
            return new EntityId(toString());
        } else {
            return new EntityId(toString() + '-' + num);
        }
    }

    public static PrototypeId parse(String str, int defaultZoneId) {
        if (str == null || "".equals(str)) {
            return null;
        }
        String[] list = str.split("-");
        int idx = 0;

        if (list.length > 2) {
            return null;
        }

        int zoneId = defaultZoneId;
        if (list.length == 2) {
            try {
                zoneId = Integer.valueOf(list[idx]);
            } catch (NumberFormatException e) {
                return null;
            }
            ++idx;
        }

        try {
            return new PrototypeId(zoneId, Integer.valueOf(list[idx]));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static PrototypeId valueOf(String str) {
        String[] list = str.split("-");
        if (list.length != 2) {
            throw new IllegalArgumentException("Expected id to be format of X-XXXXXXc, got: " + str);
        }

        int zoneId;
        try {
            zoneId = Integer.valueOf(list[0]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Expected id to be format of X-XXXXXXc, got: " + str);
        }

        int id;
        try {
            id = Integer.valueOf(list[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Expected id to be format of X-XXXXXXc, got: " + str);
        }

        return new PrototypeId(zoneId, id);
    }

    private final int zoneId;
    private final int id;
}
