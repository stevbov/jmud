package net.jmud.types;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

@EqualsAndHashCode
public class EntityId {
    public EntityId(String id) {
        this.id = id;
    }

    public EntityId() {
        id = UUID.randomUUID().toString();
    }

    public String toString() {
        return id;
    }

    public static EntityId parse(String str) {
        if (str == null) {
            return null;
        }

        return new EntityId(str);
    }

    @Getter
    private final String id;
}
