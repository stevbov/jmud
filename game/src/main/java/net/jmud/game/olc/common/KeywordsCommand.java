package net.jmud.game.olc.common;

import net.jmud.game.core.Command;
import net.jmud.game.core.CommandArgs;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Prototype;

public class KeywordsCommand extends Command {
    public KeywordsCommand(Prototype prototype) {
        super("keywords", false, true);
        this.prototype = prototype;
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        String newKeywords = args.rest();

        if (newKeywords == null) {
            commandFailed(entity, "Usage: keywords <keywords>");
            return;
        }
        prototype.setKeywords(newKeywords);
        entity.getPlayer().writeLine("Keywords set.");
    }

    private final Prototype prototype;
}
