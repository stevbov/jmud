package net.jmud.game.olc.editroom;

import net.jmud.game.core.*;
import net.jmud.types.Direction;
import net.jmud.types.PrototypeId;

public class DigCommand extends Command {
    public DigCommand(Prototype prototype) {
        super("dig", false, true);
        this.prototype = prototype;
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Direction direction = Direction.parse(args.next());
        if (direction == null) {
            commandFailed(entity, "Usage: dig <direction> <room id>");
            return;
        }

        PrototypeId id = PrototypeId.parse(args.next(), entity.findZone().getId());
        if (id == null) {
            commandFailed(entity, "Usage: dig <direction> <room id>");
            return;
        }
        if (entity.getGame().findZone(id.getZoneId()) == null) {
            commandFailed(entity, "Usage: dig <direction> <room id>");
            return;
        }

        Prototype to = entity.getGame().findRoomPrototype(id);
        Entity room;
        if (to == null) {
            to = entity.getGame().findZone(id.getZoneId()).newRoom();
            room = to.create();
            entity.getGame().addEntity(room);
        } else {
            if (to.getCreatedEntities().size() == 1) {
                room = entity.getGame().findEntity(to.getCreatedEntities().get(0));
            } else {
                room = null;
            }
        }

        prototype.setExit(new PrototypeExit(direction, id));
        to.setExit(new PrototypeExit(direction.getReverse(), prototype.getId()));
        if (room != null && entity.getLocation() != room) {
            entity.moveTo(room);
        }

        prototype.updateCreatedEntities(entity.getGame());
        entity.getPlayer().replaceState(new PlayerStateEditRoom(to));
    }

    private final Prototype prototype;
}
