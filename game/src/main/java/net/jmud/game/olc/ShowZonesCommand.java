package net.jmud.game.olc;

import net.jmud.game.core.*;

public class ShowZonesCommand extends Command {
    public ShowZonesCommand() {
        super("show zones", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Player player = entity.getPlayer();

        for (Zone zone : entity.getGame().getZones()) {
            player.writeLine("[%s] - %s", zone.getId(), zone.getName());
        }
    }
}
