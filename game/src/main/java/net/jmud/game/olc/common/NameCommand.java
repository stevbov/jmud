package net.jmud.game.olc.common;

import net.jmud.game.core.*;

public class NameCommand extends Command {
    public NameCommand(Prototype prototype) {
        super("name", false, true);
        this.prototype = prototype;
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        String name = args.rest();

        if (name == null) {
            commandFailed(entity, "Usage: name <name>");
            return;
        }
        prototype.setName(name);
        entity.getPlayer().writeLine("Name set.");
    }

    private final Prototype prototype;
}
