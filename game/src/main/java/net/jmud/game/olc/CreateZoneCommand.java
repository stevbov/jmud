package net.jmud.game.olc;

import net.jmud.game.actions.Look;
import net.jmud.game.core.*;

public class CreateZoneCommand extends Command {
    public CreateZoneCommand() {
        super("create zone", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Zone zone = entity.getGame().newZone();
        Prototype prototype = zone.newRoom();
        Entity room = prototype.create();
        entity.getGame().addEntity(room);
        entity.moveTo(room);

        Look look = new Look(entity);
        look.execute();
    }
}
