package net.jmud.game.olc.common;

import net.jmud.game.core.*;

public class DoneCommand extends Command {
    public DoneCommand(Prototype prototype) {
        super("done", false, true);
        this.prototype = prototype;
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        prototype.updateCreatedEntities(entity.getGame());
        Player player = entity.getPlayer();

        player.popState();
        player.writeLine("Done editing.");
    }

    private final Prototype prototype;
}
