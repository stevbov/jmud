package net.jmud.game.olc;

import net.jmud.game.core.*;

public class ShowRoomsCommand extends Command {
    public ShowRoomsCommand() {
        super("show rooms", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        String zoneId = args.next();
        Zone zone;

        if (zoneId == null) {
            zone = entity.findZone();
            if (zone == null) {
                commandFailed(entity, "You aren't in a zone - please provide a zone id.  Type 'show zones' to list all zones.");
                return;
            }

        } else {
            zone = entity.getGame().findZone(zoneId);
            if (zone == null) {
                commandFailed(entity, "Could not find zone with id '%s'.  Type 'show zones' to list all zones.", zoneId);
                return;
            }
        }

        Player player = entity.getPlayer();
        player.writeLine("%s [%s]: %d rooms", zone.getName(), zone.getId(), zone.getRooms().size());

        for (Prototype prototype : zone.getRooms()) {
            player.writeLine("  [%s] %s", prototype.getId(), prototype.getName());
        }
    }
}
