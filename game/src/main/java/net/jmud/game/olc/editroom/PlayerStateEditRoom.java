package net.jmud.game.olc.editroom;

import net.jmud.game.commands.CommandList;
import net.jmud.game.core.Player;
import net.jmud.game.core.PlayerState;
import net.jmud.game.core.Prototype;
import net.jmud.game.olc.common.DoneCommand;
import net.jmud.game.olc.common.KeywordsCommand;
import net.jmud.game.olc.common.NameCommand;

public class PlayerStateEditRoom implements PlayerState {
    public PlayerStateEditRoom(Prototype prototype) {
        this.prototype = prototype;
        commands.add(new DoneCommand(prototype));
        commands.add(new ShowCommand(prototype));
        commands.add(new NameCommand(prototype));
        commands.add(new KeywordsCommand(prototype));
        commands.add(new DigCommand(prototype));
    }

    @Override
    public void entered(Player p) {
        commands.runCommand(p.getEntity(), "show");
    }

    @Override
    public void left(Player p) {
    }

    @Override
    public void processInput(Player p, String input) {
        if ("".equals(input)) {
            return;
        }

        if (!commands.runCommand(p.getEntity(), input)) {
            p.writeLine("Unknown command '" + input + "'.  Type 'commands' to see all commands.");
        }
    }

    @Override
    public void prompt(Player p) {
        p.write("> ");
    }

    private final Prototype prototype;
    private final CommandList commands = new CommandList();
}
