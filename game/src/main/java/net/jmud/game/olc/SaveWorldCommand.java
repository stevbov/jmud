package net.jmud.game.olc;

import net.jmud.db.ZoneDto;
import net.jmud.game.core.*;

import java.util.List;
import java.util.stream.Collectors;

public class SaveWorldCommand extends Command {
    public SaveWorldCommand() {
        super("save world", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Player player = entity.getPlayer();
        List<ZoneDto> zones = entity.getGame().getZones().stream()
                .map(Zone::toDto)
                .collect(Collectors.toList());
        entity.getGame().getZoneDb().saveAll(zones);
        player.writeLine("World saved.");
    }
}
