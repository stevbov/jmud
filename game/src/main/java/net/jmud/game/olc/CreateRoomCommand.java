package net.jmud.game.olc;

import net.jmud.game.actions.Look;
import net.jmud.game.core.*;
import net.jmud.game.olc.editroom.PlayerStateEditRoom;
import net.jmud.types.PrototypeId;

public class CreateRoomCommand extends Command {
    public CreateRoomCommand() {
        super("create room", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Zone inZone = entity.findZone();
        String idArg = args.next();
        Prototype prototype;

        if (idArg == null) {
            prototype = inZone.newRoom();
        } else {
            PrototypeId id = PrototypeId.parse(idArg, inZone.getId());
            if (id == null) {
                commandFailed(entity, "Usage: create room <number|zoneid-number>");
                return;
            }

            Zone zone = entity.getGame().findZone(id.getZoneId());
            if (zone == null) {
                commandFailed(entity, "No zone with id %d.", id.getZoneId());
                return;
            }

            if (zone.findRoom(id) != null) {
                commandFailed(entity, "Room with id %s already exists.", id);
                return;
            }

            prototype = new Prototype(id);
            zone.addRoom(prototype);
        }

        Entity room = prototype.create();
        entity.getGame().addEntity(room);
        entity.moveTo(room);

        entity.getPlayer().pushState(new PlayerStateEditRoom(prototype));
    }
}
