package net.jmud.game.olc;

import net.jmud.game.core.*;
import net.jmud.game.olc.editroom.PlayerStateEditRoom;
import net.jmud.types.PrototypeId;

public class EditRoomCommand extends Command {
    public EditRoomCommand() {
        super("edit room", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Zone inZone = entity.findZone();
        String idStr = args.next();

        Prototype prototype;
        if (idStr != null) {
            PrototypeId id = PrototypeId.parse(args.next(), inZone.getId());
            if (id == null) {
                commandFailed(entity, "Usage: edit room <prototype id>");
                return;
            }

            prototype = entity.getGame().findRoomPrototype(id);
            if (prototype == null) {
                commandFailed(entity, "Could not find prototype with id %s.", id);
                return;
            }
        } else {
            Entity location = entity.findTopLocation();
            if (entity.getLocation() != location) {
                entity.moveTo(location);
            }
            prototype = location.getPrototype();
        }

        entity.getPlayer().pushState(new PlayerStateEditRoom(prototype));
    }
}
