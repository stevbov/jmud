package net.jmud.game.olc;

import net.jmud.game.core.*;

public class SaveZoneCommand extends Command {
    public SaveZoneCommand() {
        super("save zone", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        String zoneId = args.next();
        Player player = entity.getPlayer();
        Zone zone;

        if (zoneId == null) {
            zone = entity.findZone();
            if (zone == null) {
                commandFailed(entity, "You aren't in a zone - please provide a zone id.  Type 'show zones' to list all zones.");
                return;
            }
        } else {
            zone = entity.getGame().findZone(zoneId);
            if (zone == null) {
                commandFailed(entity, "Could not find zone with id '%s'.  Type 'show zones' to list all zones.", zoneId);
                commandFailed(entity, "Usage: save zone");
                commandFailed(entity, "       save zone all");
                commandFailed(entity, "       save zone <zone id>");
                return;
            }
        }

        entity.getGame().getZoneDb().save(zone.toDto());
        player.writeLine("Zone saved.");
    }
}
