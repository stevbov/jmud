package net.jmud.game.olc.editroom;

import net.jmud.game.core.*;

public class ShowCommand extends Command {
    public ShowCommand(Prototype prototype) {
        super("show", false, true);
        this.prototype = prototype;
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Player p = entity.getPlayer();

        p.writeLine("Id:       %s", prototype.getId());
        p.writeLine("Name:     %s", prototype.getName());
        p.writeLine("Keywords: %s", prototype.getKeywords());
        p.writeLine("Exits:");
        for (PrototypeExit exit : prototype.getExits()) {
            Prototype to = p.getGame().findRoomPrototype(exit.getTo());
            if (to == null) {
                p.writeLine("  %5s - !INVALID! (%s)", exit.getDirection().getName(), exit.getTo());
            } else {
                p.writeLine("  %5s - %s (%s)", exit.getDirection().getName(), to.getName(), exit.getTo());
            }
        }
    }

    private final Prototype prototype;
}
