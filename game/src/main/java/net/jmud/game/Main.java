package net.jmud.game;

import net.jmud.db.ZoneDb;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Game;
import net.jmud.game.telnet.TelnetServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        log.info("Starting subsystems.");

        Settings settings = new Settings(args);
        ZoneDb zoneDb = new ZoneDb(Paths.get(settings.getString("data.dir"), "zones"));

        Game game = new Game(settings, zoneDb);
        TelnetServer telnetServer = new TelnetServer(settings, game);
        telnetServer.start();
        Thread gameThread = game.start();

        log.info("Game running, waiting on shutdown.");
        try {
            synchronized (gameThread) {
                gameThread.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("Shutting down.");
    }

    private static final Logger log = LoggerFactory.getLogger(Main.class);
}
