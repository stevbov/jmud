package net.jmud.game.commands;

import net.jmud.game.actions.Look;
import net.jmud.game.core.Command;
import net.jmud.game.core.Entity;
import net.jmud.game.core.CommandArgs;
import net.jmud.types.EntityId;

public class GotoCommand extends Command {
    public GotoCommand() {
        super("goto", false, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        EntityId id = EntityId.parse(args.next());
        if (id == null) {
            commandFailed(entity, "Usage: goto <entity id>");
            return;
        }

        Entity location = entity.getGame().findEntity(id);
        if (location == null) {
            commandFailed(entity, "Could not find entity with id %s", id);
            return;
        }

        location = location.findTopLocation();
        entity.moveTo(location);
        Look look = new Look(entity);
        look.execute();
    }
}
