package net.jmud.game.commands;

import net.jmud.game.actions.Move;
import net.jmud.game.core.Command;
import net.jmud.game.core.CommandArgs;
import net.jmud.types.Direction;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Exit;

public class MoveCommand extends Command {
    public MoveCommand(Direction direction) {
        super(direction.getName(), true, true);
        this.direction = direction;
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        if (entity.getLocation() == null) {
            commandFailed(entity, "Cannot move %s.", direction.getName());
            return;
        }

        Exit exit = entity.getLocation().findExit(direction);
        if (exit == null) {
            commandFailed(entity, "Cannot move %s.", direction.getName());
            return;
        }
        Move action = new Move(entity, exit);
        action.execute();
    }

    private final Direction direction;
}
