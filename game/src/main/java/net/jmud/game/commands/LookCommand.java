package net.jmud.game.commands;

import net.jmud.game.actions.Look;
import net.jmud.game.core.Command;
import net.jmud.game.core.Entity;
import net.jmud.game.core.CommandArgs;

public class LookCommand extends Command {
    public LookCommand() {
        super("look", true, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        Look action = new Look(entity);
        String at = args.next();

        if (at == null) {
            action.execute();
        } else {
            Entity target = entity.findHere(at);

            if (target == null) {
                commandFailed(entity, "There is no '%s' here.", at);
            } else {
                action.setTarget(target);
                action.execute();
            }
        }
    }
}
