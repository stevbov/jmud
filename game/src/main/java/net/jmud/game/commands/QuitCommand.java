package net.jmud.game.commands;

import net.jmud.game.actions.Quit;
import net.jmud.game.core.Command;
import net.jmud.game.core.Entity;
import net.jmud.game.core.CommandArgs;

public class QuitCommand extends Command {
    public QuitCommand() {
        super("quit", true, true);
    }

    @Override
    protected void doExecute(Entity entity, String keywords, CommandArgs args) {
        if (!keywords.equalsIgnoreCase("quit")) {
            commandFailed(entity, "You must type 'quit' to quit.");
            return;
        }
        Quit action = new Quit(entity);
        action.execute();
    }
}
