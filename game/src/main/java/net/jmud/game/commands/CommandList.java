package net.jmud.game.commands;

import net.jmud.game.core.Command;
import net.jmud.game.core.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommandList {
    public boolean runCommand(Entity entity, String input) {
        if (entity.getPlayer() != null && "commands".equalsIgnoreCase(input)) {
            entity.getPlayer().writeLine("Valid Commands:");
            for (Command command : commands) {
                entity.getPlayer().writeLine(" %s", command.getKeywords().stream().collect(Collectors.joining(" ")));
            }
            return true;
        }
        for (Command command : commands) {
            if (!command.canUse(entity)) {
                continue;
            }
            if (command.matches(input)) {
                command.execute(entity, input);
                return true;
            }
        }

        return false;
    }

    public void add(Command command) {
        commands.add(command);
    }

    private List<Command> commands = new ArrayList<>();
}
