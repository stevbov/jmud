package net.jmud.game.telnet;

import lombok.Getter;
import net.jmud.game.core.PlayerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

class TelnetClient implements PlayerClient {
    public TelnetClient(Socket sock) {
        this.sock = sock;
        ip = sock.getInetAddress();
        log = LoggerFactory.getLogger(this.getClass() + " " + ip.toString().replaceAll("\\.", "_"));
    }

    public void start() throws SocketException {
        sock.setSoTimeout(1000);

        Thread thread = new Thread(this::runReadInput);
        thread.setName("Ip " + ip + " Telnet Input Reader");
        thread.start();

        thread = new Thread(this::runWriteOutput);
        thread.setName("Ip " + ip + " Telnet Output Writer");
        thread.start();
    }

    public boolean isConnected() {
        return sock != null;
    }

    public void disconnect() {
        if (isConnected()) {
            try {
                log.debug("disconnecting");
                sock.close();
            } catch (IOException e) {
                log.debug("disconnect exception: {}", e, e);
            } finally {
                sock = null;
            }
        }
    }

    private void runReadInput() {
        try {
            InputStream is = sock.getInputStream();
            byte[] buf = new byte[256];

            while (true) {
                try {
                    int len = is.read(buf);

                    if (len == -1) {
                        sock = null;
                        log.info("disconnected: sock.read returned -1");
                        break;
                    }

                    for (int i = 0; i < len; ++i) {
                        byte c = buf[i];

                        if (c == '\r' || c == '\n') {
                            if (last != '\r' && last != '\n') {
                                String newInput = input.toString();
                                log.debug("new input {}.", newInput);
                                input = new StringBuilder();
                                inputQueue.offer(newInput);
                                last = c;
                            } else {
                                last = 0;
                            }
                        } else {
                            last = c;
                            input.append((char) c);
                        }
                    }
                } catch (SocketTimeoutException e) {
                    // blank
                }

                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
            }
        } catch (IOException e) {
            log.debug("read input exception: {}", e, e);
        }
        log.debug("read input stopping");
    }

    private void runWriteOutput() {
        try {
            OutputStream out = sock.getOutputStream();

            while (true) {
                try {
                    String str = output.poll(1, TimeUnit.SECONDS);

                    if (str != null) {
                        log.trace("writing and flushing output");
                        out.write(str.getBytes());
                        out.flush();
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    break;
                }

                if (!isConnected()) {
                    log.debug("write output: no longer connected");
                    break;
                }
            }
        } catch (IOException e) {
            log.debug("write output exception: {}", e, e);
        }
        log.debug("write output stopping");
    }

    public void send(String str) {
        log.trace("adding output");
        output.offer(str);
    }

    public String nextInput() {
        return inputQueue.poll();
    }

    private Socket sock;
    @Getter
    private InetAddress ip;
    private StringBuilder input = new StringBuilder();
    private final BlockingQueue<String> output = new LinkedBlockingQueue<>();
    private final ConcurrentLinkedQueue<String> inputQueue = new ConcurrentLinkedQueue<>();
    private byte last = 0;

    private final Logger log;
}
