package net.jmud.game.telnet.states;

import net.jmud.game.core.Player;
import net.jmud.game.core.PlayerName;
import net.jmud.game.core.PlayerState;

public class PlayerStateConfirmName implements PlayerState {
    public PlayerStateConfirmName(PlayerName name) {
        this.name = name;
    }

    @Override
    public void entered(Player p) {
    }

    @Override
    public void left(Player p) {
    }

    @Override
    public void processInput(Player p, String input) {
        if ("".equals(input)) {
            return;
        }

        if ("yes".startsWith(input.toLowerCase())) {
            p.replaceState(new PlayerStateClass(name));
        } else if ("no".startsWith(input.toLowerCase())) {
            p.replaceState(new PlayerStateName());
        }
    }

    @Override
    public void prompt(Player p) {
        p.write(name.getName() + " - are you sure? (Y/N)");
    }

    private final PlayerName name;
}
