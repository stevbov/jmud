package net.jmud.game.telnet;

import net.jmud.game.Settings;
import net.jmud.game.core.Game;
import net.jmud.game.core.Player;
import net.jmud.game.telnet.states.PlayerStateName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class TelnetServer implements Runnable {
    public TelnetServer(Settings settings, Game game) {
        port = settings.getInt("port");
        this.game = game;
    }

    public void start() throws IOException {
        log.info("Starting up.", port);

        sock = new ServerSocket(port);
        sock.setSoTimeout(1000);
        Thread thread = new Thread(this);
        thread.setName("Telnet Server");
        thread.start();
    }

    @Override
    public void run() {
        log.info("Running on port {}.", port);
        while (true) {
            try {
                Socket con = sock.accept();

                log.info("New connection from ip [{}] ", con.getInetAddress());
                TelnetClient client = new TelnetClient(con);
                Player player = new Player(client, new PlayerStateName(), game);
                client.start();
                game.newPlayer(player);
            } catch (SocketTimeoutException e) {
                // blank on purpose
            } catch (IOException e) {
                log.error("Failed to accept socket: {}", e.getMessage(), e);
            }

            if (Thread.currentThread().isInterrupted()) {
                break;
            }
        }

        log.info("Shutting down");
        try {
            sock.close();
        } catch (IOException e) {
            log.warn("Failed to close socket: {}", e.getMessage(), e);
        }
    }

    private ServerSocket sock;
    private final int port;
    private final Game game;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
}
