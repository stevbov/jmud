package net.jmud.game.telnet.states;

import net.jmud.game.core.Player;
import net.jmud.game.core.PlayerName;
import net.jmud.game.core.PlayerState;

public class PlayerStateName implements PlayerState {
    @Override
    public void entered(Player p) {
        p.writeLine("Welcome to the game!");
    }

    @Override
    public void left(Player p) {
    }

    @Override
    public void processInput(Player p, String input) {
        if ("".equals(input)) {
            return;
        }

        String error = PlayerName.validate(input);
        if (error != null) {
            p.writeLine(error);
            return;
        }
        p.replaceState(new PlayerStateConfirmName(new PlayerName(input)));
    }

    @Override
    public void prompt(Player p) {
        p.write("What is your character's name? ");
    }
}
