package net.jmud.game.telnet.states;

import net.jmud.game.commands.*;
import net.jmud.game.core.Player;
import net.jmud.game.core.PlayerState;
import net.jmud.game.olc.*;
import net.jmud.types.Direction;

public class PlayerStatePlaying implements PlayerState {
    @Override
    public void entered(Player p) {
    }

    @Override
    public void left(Player p) {
    }

    @Override
    public void processInput(Player p, String input) {
        if ("".equals(input)) {
            return;
        }

        if (!commands.runCommand(p.getEntity(), input)) {
            p.writeLine("Unknown command '" + input + "'.");
        }
    }

    @Override
    public void prompt(Player p) {
        p.write("> ");
    }

    private static final CommandList commands = new CommandList();
    static {
        for (Direction direction : Direction.values()) {
            commands.add(new MoveCommand(direction));
        }
        commands.add(new QuitCommand());
        commands.add(new LookCommand());
        commands.add(new GotoCommand());
        commands.add(new ShowRoomsCommand());
        commands.add(new ShowZonesCommand());
        commands.add(new CreateRoomCommand());
        commands.add(new CreateZoneCommand());
        commands.add(new EditRoomCommand());
        commands.add(new SaveZoneCommand());
        commands.add(new SaveWorldCommand());
    }
}
