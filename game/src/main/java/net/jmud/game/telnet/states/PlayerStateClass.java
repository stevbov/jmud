package net.jmud.game.telnet.states;

import net.jmud.game.actions.Look;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Player;
import net.jmud.game.core.PlayerName;
import net.jmud.game.core.PlayerState;
import net.jmud.types.EntityId;

public class PlayerStateClass implements PlayerState {
    public PlayerStateClass(PlayerName name) {
        this.name = name;
    }

    @Override
    public void entered(Player p) {
    }

    @Override
    public void left(Player p) {
    }

    @Override
    public void processInput(Player p, String input) {
        if ("".equals(input)) {
            entered(p);
            return;
        }

        int choice;
        try {
            choice = Integer.valueOf(input);
            if (choice < 1 || choice > classes.length) {
                p.writeLine("Please select a number from 1 to " + classes.length);
                entered(p);
                return;
            }
        } catch (NumberFormatException e) {
            p.writeLine("Please select a number from 1 to " + classes.length);
            entered(p);
            return;
        }

        Entity entity = new Entity(new EntityId(), p, null);
        entity.setName(name.getName());
        entity.setKeywords(name.getName());
        p.setEntity(entity);
        p.replaceState(new PlayerStatePlaying());
        p.getGame().addEntity(entity);
        entity.moveTo(p.getGame().getStartingLocation());

        Look look = new Look(entity);
        look.execute();
    }

    @Override
    public void prompt(Player p) {
        for (int i = 0; i < classes.length; ++i) {
            p.write("(%d) %-10s", (i + 1), classes[i]);
        }
        p.writeLine("");
        p.write("Please select a class: ");
    }

    private static final String[] classes = new String[]{"Fighter", "Rogue", "Cleric", "Mage"};
    private final PlayerName name;
}
