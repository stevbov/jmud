package net.jmud.game.actions;

import net.jmud.game.core.Action;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Exit;
import net.jmud.game.core.Player;

public class Moved extends Action {
    public Moved(Entity actor, Exit exit) {
        super(actor);
        visibility.add(Visibility.ACTOR_ROOM);
        this.exit = exit;
    }

    @Override
    protected void calculate() {
    }

    @Override
    protected void apply() {
        Look look = new Look(actor);
        look.execute();
    }

    @Override
    public void perceivedByPlayer(Entity entity, Player player) {
        if (entity != actor) {
            player.writeLine("%s came in from the %s.", actor.getName(), exit.getDirection().getReverse().getName());
        }
    }

    @Override
    public void perceivedByNpc(Entity entity) {
    }

    private final Exit exit;
}
