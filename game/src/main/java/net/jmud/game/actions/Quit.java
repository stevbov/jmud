package net.jmud.game.actions;

import net.jmud.game.core.Action;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Player;

public class Quit extends Action {
    public Quit(Entity actor) {
        super(actor);
        visibility.add(Visibility.ACTOR_ROOM);
    }

    @Override
    protected void calculate() {
    }

    @Override
    protected void apply() {
        getActor().getPlayer().disconnect();
    }

    @Override
    public void perceivedByPlayer(Entity entity, Player player) {
        if (entity == actor) {
            player.writeLine("Goodbye!");
        } else {
            player.writeLine("%s has quit.", actor.getName());
        }
    }

    @Override
    public void perceivedByNpc(Entity entity) {
    }
}
