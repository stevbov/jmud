package net.jmud.game.actions;

import net.jmud.game.core.Action;
import net.jmud.game.core.Entity;
import net.jmud.game.core.Exit;
import net.jmud.game.core.Player;

import java.util.stream.Collectors;

public class Look extends Action {
    public Look(Entity actor) {
        super(actor);
    }

    @Override
    protected void calculate() {
        if (target != null) {
            visibility.add(Visibility.TARGET);
            visibility.add(Visibility.BOTH_ROOM);
        }
    }

    @Override
    protected void apply() {
    }

    @Override
    public void perceivedByPlayer(Entity entity, Player player) {
        if (target == null) {
            Entity location = entity.getLocation();

            if (location != null) {
                player.writeLine("%s", location.getName());
                for (Entity e : location.getContents()) {
                    if (!e.equals(entity)) {
                        player.writeLine("%s is here.", e.getName());
                    }
                }

                player.writeLine("[%s]", location.getExits().stream()
                        .map(e -> e.getDirection().getName())
                        .collect(Collectors.joining(" ")));
            } else {
                player.writeLine("You are in the void.");
            }
        } else if (target.equals(actor))  {
            if (entity.equals(actor)) {
                player.writeLine("You look at yourself.");
            } else {
                player.writeLine("%s looks at themself.", actor.getName());
            }
        } else {
            if (entity.equals(actor)) {
                player.writeLine("You look at %s.", target.getName());
            } else if (entity.equals(target)) {
                player.writeLine("%s looks at you.", actor.getName());
            } else {
                player.writeLine("%s looks at %s.", actor.getName(), target.getName());
            }
        }
    }

    @Override
    public void perceivedByNpc(Entity entity) {
    }
}
