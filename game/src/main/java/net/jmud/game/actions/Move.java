package net.jmud.game.actions;

import net.jmud.game.core.*;

public class Move extends Action {
    public Move(Entity actor, Exit exit) {
        super(actor);
        visibility.add(Visibility.ACTOR_ROOM);
        this.exit = exit;
    }

    @Override
    protected void calculate() {
    }

    @Override
    protected void apply() {
        actor.moveTo(exit.getTo());
        Moved moved = new Moved(actor, exit);
        moved.execute();
    }

    @Override
    public void perceivedByPlayer(Entity entity, Player player) {
        if (entity != actor) {
            player.writeLine("%s left %s.", actor.getName(), exit.getDirection().getName());
        } else {
            player.writeLine("You went %s.", exit.getDirection().getName());
        }
    }

    @Override
    public void perceivedByNpc(Entity entity) {
    }

    private final Exit exit;
}
