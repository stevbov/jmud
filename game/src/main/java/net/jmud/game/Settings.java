package net.jmud.game;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {
    public Settings(String[] args) throws IOException {
        props = new Properties();

        try (InputStream in = this.getClass().getClassLoader().getResourceAsStream(SETTINGS_FILE)) {
            if (in != null) {
                props.load(in);
            }
        }

        try (InputStream in = new FileInputStream(SETTINGS_FILE)) {
            props.load(in);
        } catch (IOException e) {
            // ignore
        }

        props.putAll(System.getProperties());
        props.putAll(System.getenv());

        for (String arg : args) {
            if (arg.startsWith("-P")) {
                String original = arg;

                arg = arg.substring(2);
                String[] argList = arg.split("=", 2);

                if (argList.length != 2) {
                    throw new IOException("property argument '" + original + "' must be of form -P<key>=<value>");
                }

                props.put(argList[0], argList[1]);
            }
        }
    }


    public String getString(String key) {
        return props.getProperty(key);
    }

    public int getInt(String key) {
        return Integer.valueOf(getString(key));
    }

    public long getLong(String key) {
        return Long.valueOf(getString(key));
    }

    private final Properties props;
    private static final String SETTINGS_FILE = "game.properties";
}
