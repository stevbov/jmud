package net.jmud.game.core;

public class CommandArgs {
    public CommandArgs(String input) {
        original = input;
        current = null;
        rest = original;
    }

    public String next() {
        if (rest == null) {
            current = null;
            return current;
        }

        int idx = rest.indexOf(' ');
        if (idx == -1) {
            idx = rest.length();
        }

        current = rest.substring(0, idx);

        if (idx == rest.length()) {
            rest = null;
        } else {
            rest = rest.substring(idx).trim();
            if (rest.isEmpty()) {
                rest = null;
            }
        }

        return current;
    }

    public String current() {
        return current;
    }

    public String rest() {
        current = rest;
        rest = null;
        return current;
    }

    private final String original;
    private String current;
    private String rest;
}
