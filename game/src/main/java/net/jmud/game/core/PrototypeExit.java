package net.jmud.game.core;

import lombok.Data;
import net.jmud.db.PrototypeExitDto;
import net.jmud.types.Direction;
import net.jmud.types.PrototypeId;

@Data
public class PrototypeExit {
    public PrototypeExit(Direction direction, PrototypeId to) {
        this.direction = direction;
        this.to = to;
    }

    public PrototypeExit(PrototypeExitDto dto) {
        direction = dto.getDirection();
        to = dto.getTo();
    }

    public PrototypeExitDto toDto() {
        PrototypeExitDto dto = new PrototypeExitDto();
        dto.setDirection(direction);
        dto.setTo(to);
        return dto;
    }

    private Direction direction;
    private PrototypeId to;
}
