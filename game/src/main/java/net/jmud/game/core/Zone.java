package net.jmud.game.core;

import lombok.Getter;
import lombok.Setter;
import net.jmud.db.ZoneDto;
import net.jmud.types.PrototypeId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Zone {
    public Zone(int id) {
        this.id = id;
        rooms = new ArrayList<>();
        name = "New Zone";
    }

    public Zone(ZoneDto dto) {
        id = dto.getId();
        name = dto.getName();
        rooms = dto.getRooms().stream()
                .map(Prototype::new)
                .collect(Collectors.toList());
        prototypes.addAll(rooms);
    }

    public Prototype newRoom() {
        Prototype room = new Prototype(new PrototypeId(id, nextPrototypeId()));
        rooms.add(room);
        prototypes.add(room);
        return room;
    }

    public List<Entity> createRooms() {
        List<Entity> rooms = new ArrayList<>();
        for (Prototype roomProto : this.rooms) {
            Entity room = roomProto.create();

            rooms.add(room);
        }
        return rooms;
    }

    public List<Prototype> getRooms() {
        return Collections.unmodifiableList(rooms);
    }

    public void addRoom(Prototype room) {
        if (prototypes.stream().anyMatch(r -> r.getId().equals(room.getId()))) {
            throw new IllegalArgumentException("Prototype id " + room.getId() + " already exists.");
        }
        if (room.getId().getZoneId() != id) {
            throw new IllegalArgumentException("Prototype id " + room.getId() + " does not match zone id " + id);
        }
        rooms.add(room);
    }

    public Prototype findRoom(PrototypeId id) {
        return rooms.stream()
                .filter(r -> r.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    public ZoneDto toDto() {
        ZoneDto dto = new ZoneDto();
        dto.setId(id);
        dto.setName(name);
        dto.setRooms(rooms.stream().map(Prototype::toDto).collect(Collectors.toList()));
        return dto;
    }

    private int nextPrototypeId() {
        int prototypeId = 1;
        boolean found;

        do {
            found = false;

            for (Prototype prototype : prototypes) {
                if (prototype.getId().getId() == prototypeId) {
                    found = true;
                    ++prototypeId;
                    break;
                }
            }
        } while (found);

        return prototypeId;
    }

    @Getter
    private final int id;
    private final List<Prototype> rooms;
    private final List<Prototype> prototypes = new ArrayList<>();
    @Getter @Setter
    private String name;
}
