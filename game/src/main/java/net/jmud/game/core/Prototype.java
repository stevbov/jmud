package net.jmud.game.core;

import lombok.Data;
import lombok.Getter;
import net.jmud.db.PrototypeDto;
import net.jmud.db.PrototypeExitDto;
import net.jmud.types.Direction;
import net.jmud.types.EntityId;
import net.jmud.types.PrototypeId;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Prototypes are used to configure entities and load multiples of them.  Mostly used for building zones.
 */
@Data
public class Prototype {
    public Prototype(PrototypeId id) {
        this.id = id;
        name = "New Prototype";
        keywords = "new prototype";
    }

    public Prototype(PrototypeDto dto) {
        id = dto.getId();
        name = dto.getName();
        keywords = dto.getKeywords();
        for (PrototypeExitDto exitDto : dto.getExits()) {
            exits.add(new PrototypeExit(exitDto));
        }
    }

    public Entity create() {
        Entity entity = new Entity(nextEntityId(), this);

        entity.setName(name);
        entity.setKeywords(keywords);

        createdEntities.add(entity.getId());

        return entity;
    }

    private EntityId nextEntityId() {
        ++numCreated;
        return id.toEntityId(numCreated);
    }


    public PrototypeDto toDto() {
        PrototypeDto dto = new PrototypeDto();
        dto.setId(id);
        dto.setName(name);
        dto.setKeywords(keywords);
        dto.setExits(exits.stream().map(PrototypeExit::toDto).collect(Collectors.toList()));
        return dto;
    }

    public void updateCreatedEntities(Game game) {
        for (Iterator<EntityId> i = createdEntities.iterator(); i.hasNext(); ) {
            Entity entity = game.findEntity(i.next());
            if (entity == null) {
                i.remove();
                continue;
            }

            entity.setName(name);
            entity.setKeywords(keywords);

            for (PrototypeExit exit : exits) {
                Exit existing = entity.findExit(exit.getDirection());
                if (existing == null || !existing.getTo().getPrototype().getId().equals(exit.getTo())) {
                    Prototype to = game.findRoomPrototype(exit.getTo());
                    Entity toEntity = game.findEntity(to.getCreatedEntities().get(0));
                    entity.setExit(exit.getDirection(), toEntity);
                }
            }

            for (int j = 0; j < entity.getExits().size(); ) {
                Exit exit = entity.getExits().get(j);
                if (exits.stream().noneMatch(e -> e.getDirection() == exit.getDirection())) {
                    entity.removeExit(exit.getDirection());
                } else {
                    ++j;
                }

            }
        }
    }

    public void removeExit(Direction direction) {
        exits.removeIf(e -> e.getDirection() == direction);
    }

    public List<PrototypeExit> getExits() {
        return Collections.unmodifiableList(exits);
    }

    public void setExit(PrototypeExit exit) {
        removeExit(exit.getDirection());
        exits.add(exit);
        exits.sort(Comparator.comparing(PrototypeExit::getDirection));
    }

    private final PrototypeId id;
    private String name;
    private String keywords;
    private int numCreated = 0;
    private final List<PrototypeExit> exits = new ArrayList<>();
    private final List<EntityId> createdEntities = new ArrayList<>();
}
