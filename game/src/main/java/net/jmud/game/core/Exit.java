package net.jmud.game.core;

import lombok.Getter;
import net.jmud.types.Direction;

public class Exit {
    public Exit(Direction direction, Entity to) {
        this.direction = direction;
        this.to = to;
    }

    @Getter
    private final Direction direction;
    @Getter
    private final Entity to;
}
