package net.jmud.game.core;

import lombok.Data;

@Data
public class PlayerName {
    public PlayerName(String name) {
        if (validate(name) != null) {
            throw new IllegalArgumentException("Name [" + name + "] not valid.");
        }
        name = name.toLowerCase();
        name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        this.name = name;
    }

    private final String name;

    public static String validate(String name) {
        if (!name.matches("[a-zA-Z]+")) {
            return "A name can only contain alphabetical characters.";
        }

        if (name.length() < 2 || name.length() > 15) {
            return "A name must be between 2 and 15 characters long.";
        }

        return null;
    }
}
