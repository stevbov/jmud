package net.jmud.game.core;

public interface PlayerState {
    void entered(Player p);
    void left(Player p);
    void processInput(Player p, String input);
    void prompt(Player p);
}
