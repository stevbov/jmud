package net.jmud.game.core;

import com.google.common.collect.ImmutableSet;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import net.jmud.types.Direction;
import net.jmud.types.EntityId;

import java.util.*;

@EqualsAndHashCode(of="id")
public class Entity {
    public Entity(EntityId id, Prototype prototype) {
        this.id = id;
        player = null;
        this.prototype = prototype;
    }

    public Entity(EntityId id, Player player, Prototype prototype) {
        this.id = id;
        this.player = player;
        this.prototype = prototype;
    }

    public void moveTo(Entity location) {
        if (this.location != null) {
            this.location.contents.remove(this);
        }

        this.location = location;
        if (location != null) {
            location.contents.add(this);
        }
    }

    public Entity findTopLocation() {
        if (location == null) {
            return this;
        }

        return location.findTopLocation();
    }

    public List<Entity> getContents() {
        return Collections.unmodifiableList(contents);
    }

    void setGame(Game game) {
        if (this.game != null) {
            throw new IllegalStateException("Game already set.");
        }
        this.game = game;
    }

    public boolean isValid() {
        return name != null && !"".equals(name)
                && !keywords.isEmpty();
    }

    @Override
    public String toString() {
        return id.getId();
    }

    public Entity findHere(String keywords) {
        int matchNumber = 0;
        int matchDesired = 1;
        String[] keywordList = keywords.toLowerCase().split("\\.");

        int firstDot = keywords.indexOf('.');
        if (firstDot != -1) {
            String start = keywords.substring(0, firstDot);
            try {
                matchDesired = Integer.valueOf(start);
                if (matchDesired > 0) {
                    keywordList = keywords.substring(firstDot + 1).toLowerCase().split("\\.");
                } else {
                    matchDesired = 1;
                }
            } catch (NumberFormatException e) {
                // blank
            }
        }

        for (Entity target : contents) {
            if (target.matches(keywords, keywordList)) {
                if (++matchNumber == matchDesired) {
                    return target;
                }
            }
        }

        if (location != null) {
            for (Entity target : location.getContents()) {
                if (target.matches(keywords, keywordList)) {
                    if (++matchNumber == matchDesired) {
                        return target;
                    }
                }
            }
        }

        return null;
    }

    public boolean matches(String keywords, String[] keywordList) {
        if (keywords.equals(id.getId())) {
            return true;
        }

        for (String keyword : keywordList) {
            if (!this.keywords.contains(keyword)) {
                return false;
            }
        }
        return true;
    }

    public void setKeywords(String keywords) {
        this.keywords = ImmutableSet.copyOf(keywords.toLowerCase().split(" "));
    }

    public List<Exit> getExits() {
        return Collections.unmodifiableList(exits);
    }

    public void setExit(Direction direction, Entity to) {
        exits.removeIf(e -> e.getDirection() == direction);
        exits.add(new Exit(direction, to));
        exits.sort(Comparator.comparing(Exit::getDirection));
    }

    public void removeExit(Direction direction) {
        exits.removeIf(e -> e.getDirection() == direction);
    }

    public Exit findExit(Direction direction) {
        return exits.stream()
                .filter(e -> e.getDirection() == direction)
                .findAny()
                .orElse(null);

    }

    public Zone findZone() {
        if (location != null) {
            return location.findZone();
        }
        if (prototype != null) {
            return game.findZone(prototype.getId().getZoneId());
        }

        return null;
    }


    @Getter
    private final EntityId id;
    @Getter
    private Game game = null;
    @Getter
    private final Player player;
    @Getter @Setter
    private String name;
    @Getter
    private ImmutableSet<String> keywords = ImmutableSet.of();
    @Getter
    private Entity location = null;
    private final List<Entity> contents = new ArrayList<>();
    @Getter
    private final List<Exit> exits = new ArrayList<>();
    @Getter
    private final Prototype prototype;

}
