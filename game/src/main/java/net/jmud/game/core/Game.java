package net.jmud.game.core;

import lombok.Getter;
import net.jmud.db.ZoneDb;
import net.jmud.db.ZoneDto;
import net.jmud.game.Settings;
import net.jmud.types.EntityId;
import net.jmud.types.PrototypeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Game implements Runnable {
    public Game(Settings settings, ZoneDb zoneDb) {
        tickMillis = settings.getLong("tickMillis");
        this.zoneDb = zoneDb;

        loadZones();
        createRooms();
        linkRooms();

        startingLocation = findEntity(new EntityId(settings.getString("startingLocation")));
        if (startingLocation == null) {
            throw new IllegalStateException("Could not find starting location " + settings.getString("startingLocation"));
        }
    }

    private void loadZones() {
        List<ZoneDto> zoneDtos = zoneDb.loadAll();

        for (ZoneDto zoneDto : zoneDtos) {
            Zone zone = new Zone(zoneDto);
            log.info("Loaded zone [{}] [{}].", zone.getId(), zone.getName());
            zones.add(zone);
        }
    }

    private void createRooms() {
        for (Zone zone : zones) {
            for (Entity room : zone.createRooms()) {
                addEntity(room);
            }
        }
    }

    private void linkRooms() {
        for (Entity entity : entities) {
            for (PrototypeExit exitProto : entity.getPrototype().getExits()) {
                Entity to = findEntity(exitProto.getTo().firstEntityId());

                if (to == null) {
                    throw new IllegalStateException("Entity " + entity.getId() + ": could not find 'to' entity for prototype exit: " + exitProto);
                }
                entity.setExit(exitProto.getDirection(), to);
            }
        }
    }

    public Thread start() {
        Thread thread = new Thread(this);
        thread.setName("Game");
        thread.start();
        return thread;
    }

    @Override
    public void run() {
        while (true) {
            long start = System.currentTimeMillis();

            newPlayers.drainTo(players);

            for (Iterator<Player> i = players.iterator(); i.hasNext(); ) {
                Player player = i.next();
                if (!player.isConnected()) {
                    log.info("removing disconnected player");
                    i.remove();
                }
            }

            for (Player player : players) {
                player.processInput();
            }

            for (Player player : players) {
                player.processOutput();
            }

            long end = System.currentTimeMillis();
            long tickTime = (end - start);
            if (tickTime < tickMillis) {
                try {
                    Thread.sleep(tickMillis - tickTime);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }

            if (Thread.currentThread().isInterrupted()) {
                break;
            }
        }
    }

    public void newPlayer(Player player) {
        newPlayers.offer(player);
    }

    public List<Player> getPlayers() {
        return Collections.unmodifiableList(players);
    }

    public void addEntity(Entity entity) {
        if (!entity.isValid()) {
            throw new IllegalArgumentException("Attempted to add invalid entity " + entity + " to game.");
        }
        entity.setGame(this);
        entities.add(entity);
    }

    public Entity findEntity(EntityId id) {
        for (Entity entity : entities) {
            if (entity.getId().equals(id)) {
                return entity;
            }
        }
        return null;
    }

    public Zone findZone(String idStr) {
        try {
            return findZone(Integer.valueOf(idStr));
        } catch (NumberFormatException e) {
            // blank
        }
        return null;
    }

    public Zone findZone(int id) {
        for (Zone zone : zones) {
            if (zone.getId() == id) {
                return zone;
            }
        }
        return null;
    }

    public List<Zone> getZones() {
        return Collections.unmodifiableList(zones);
    }

    public Zone newZone() {
        Zone zone = new Zone(nextZoneId());
        zones.add(zone);
        return zone;
    }

    public Prototype findRoomPrototype(PrototypeId id) {
        Zone zone = findZone(id.getZoneId());
        if (zone == null) {
            return null;
        }

        return zone.findRoom(id);
    }

    private int nextZoneId() {
        return zones.stream().map(Zone::getId).max(Integer::compareTo).orElse(0) + 1;
    }

    private final long tickMillis;
    private final BlockingQueue<Player> newPlayers = new LinkedBlockingQueue<>();
    private final List<Player> players = new ArrayList<>();
    private final List<Entity> entities = new ArrayList<>();
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Getter
    private final Entity startingLocation;
    private final List<Zone> zones = new ArrayList<>();
    @Getter
    private final ZoneDb zoneDb;
}
