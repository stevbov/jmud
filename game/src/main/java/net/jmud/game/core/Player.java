package net.jmud.game.core;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Stack;

public class Player {
    public Player(PlayerClient client, PlayerState startState, Game game) {
        this.client = client;
        this.game = game;
        log = LoggerFactory.getLogger(this.getClass() + " " + client.getIp().toString().replaceAll("\\.", "_"));
        replaceState(startState);
    }

    public void replaceState(PlayerState state) {
        if (!states.empty()) {
            PlayerState current = states.pop();
            current.left(this);
        }
        states.push(state);
        state.entered(this);
    }

    public void pushState(PlayerState state) {
        if (!states.isEmpty()) {
            states.peek().left(this);
        }
        states.push(state);
        state.entered(this);
    }

    public void popState() {
        PlayerState current = states.pop();
        current.left(this);
        states.peek().entered(this);
    }

    public void processInput() {
        String input = client.nextInput();
        if (input == null) {
            log.trace("process input found nothing to run");
            return;
        }

        log.debug("processing input {}", input);
        hadInput = true;
        PlayerState state = states.peek();
        state.processInput(this, input);
    }

    public void processOutput() {
        if (hadInput || output.length() > 0) {
            writeLine("");
            states.peek().prompt(this);
        }
        if (output.length() > 0) {
            if (!hadInput) {
                client.send("\r\n");
            }
            log.debug("flushing output");
            client.send(output.toString());
            output = new StringBuilder();
        } else {
            log.trace("not flushing output");
        }
        hadInput = false;
    }

    public boolean isConnected() {
        return client.isConnected();
    }

    public void disconnect() {
        log.info("disconnecting");
        client.disconnect();
    }

    public void write(String format, Object... args) {
        output.append(String.format(format, args));
    }

    public void writeLine(String format, Object... args) {
        output.append(String.format(format, args));
        output.append("\r\n");
    }

    private PlayerClient client;
    private StringBuilder output = new StringBuilder();
    private final Logger log;
    @Getter
    private final Game game;
    @Getter @Setter
    private Entity entity;
    private boolean hadInput = false;
    private Stack<PlayerState> states = new Stack<>();
}
