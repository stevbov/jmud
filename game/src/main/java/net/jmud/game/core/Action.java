package net.jmud.game.core;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

public abstract class Action {
    public Action(Entity actor) {
        this.actor = actor;
    }

    public void execute() {
        calculate();
        notifyEntities();
        apply();
    }

    public void perceivedBy(Entity entity) {
        if (entity.getPlayer() != null) {
            perceivedByPlayer(entity, entity.getPlayer());
        } else {
            perceivedByNpc(entity);
        }
    }

    private void notifyEntities() {
        notifyEntity(actor);
        if (target != null && visibility.contains(Visibility.TARGET) && !notified.contains(target)) {
            notifyEntity(target);
        }
        if (visibility.contains(Visibility.ACTOR_ROOM) || visibility.contains(Visibility.BOTH_ROOM)) {
            for (Entity entity : actor.getLocation().getContents()) {
                notifyEntity(entity);
            }
        }

        if (target != null
                && (visibility.contains(Visibility.TARGET_ROOM) || visibility.contains(Visibility.BOTH_ROOM))) {
            for (Entity entity : target.getLocation().getContents()) {
                notifyEntity(entity);
            }
        }

        if (visibility.contains(Visibility.ALL_PLAYERS)) {
            for (Player player : actor.getGame().getPlayers()) {
                notifyEntity(player.getEntity());
            }
        }
    }

    private void notifyEntity(Entity entity) {
        if (!notified.contains(entity)) {
            perceivedBy(entity);
            notified.add(entity);
        }
    }

    protected abstract void calculate();
    protected abstract void apply();

    protected abstract void perceivedByPlayer(Entity entity, Player player);
    protected abstract void perceivedByNpc(Entity entity);

    @Getter
    protected Entity actor;
    @Getter @Setter
    protected Entity target;
    protected final Set<Visibility> visibility = new HashSet<>();
    protected final Set<Entity> notified = new HashSet<>();

    public enum Visibility {
        TARGET,
        BOTH_ROOM,
        TARGET_ROOM,
        ACTOR_ROOM,
        ALL_PLAYERS
    }
}
