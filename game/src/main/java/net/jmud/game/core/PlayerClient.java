package net.jmud.game.core;

import java.net.InetAddress;

public interface PlayerClient {
    void send(String str);
    String nextInput();
    boolean isConnected();
    void disconnect();
    InetAddress getIp();
}
