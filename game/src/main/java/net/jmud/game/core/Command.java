package net.jmud.game.core;

import com.google.common.collect.ImmutableList;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

public abstract class Command {
    public Command(String keywords, boolean canNpc, boolean canPlayer) {
        this.keywords = ImmutableList.copyOf(keywords.split(" "));
        this.canNpc = canNpc;
        this.canPlayer = canPlayer;
    }

    public void execute(Entity entity, String input) {
        CommandArgs args = new CommandArgs(input);
        String keywords = "";
        for (int i = 0; i < this.keywords.size(); ++i) {
            if (i != 0) {
                keywords += " ";
            }
            keywords += args.next();
        }

        doExecute(entity, keywords, args);
    }

    protected abstract void doExecute(Entity entity, String keywords, CommandArgs args);

    protected void commandFailed(Entity entity, String format, Object... args) {
        if (entity.getPlayer() != null) {
            entity.getPlayer().writeLine(format, args);
        } else {
            // TODO: log this?
            throw new UnsupportedOperationException("TODO");
        }
    }

    public boolean matches(String input) {
        String[] words = input.split(" ", keywords.size() + 1);
        if (words.length < keywords.size()) {
            return false;
        }

        for (int i = 0; i < keywords.size(); ++i) {
            String keyword = keywords.get(i);
            String word = words[i];

            if (!keyword.startsWith(word.toLowerCase())) {
                return false;
            }
        }

        return true;
    }

    public boolean canUse(Entity entity) {
        if (!canNpc && entity.getPlayer() == null) {
            return false;
        }
        if (!canPlayer && entity.getPlayer() != null) {
            return false;
        }
        return true;
    }

    @Getter
    private final ImmutableList<String> keywords;
    private final boolean canNpc;
    private final boolean canPlayer;
}
